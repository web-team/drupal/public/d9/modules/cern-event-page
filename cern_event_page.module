<?php

use Drupal\views\Plugin\views\cache\CachePluginBase;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;

/**
 * Implements hook_form_alter() to add classes to the search form.
 */
function cern_event_page_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  if ($form_id == 'node_event_page_edit_form' || $form_id == 'node_event_page_form') {
    $form['field_p_event_display_cds']['#states'] = [
        'visible' => [
            'select[name="field_p_event_display_media_type"]' => ['value' => 'cds']
        ]
    ];

    $form['field_p_event_display_image']['#states'] = [
        'visible' => [
            'select[name="field_p_event_display_media_type"]' => ['value' => 'image']
        ]
    ];

    $form['field_p_event_display_caption']['#states'] = [
        'visible' => [
            'select[name="field_p_event_display_media_type"]' => ['value' => 'image']
        ]
    ];
  }
}


/**
 * Implements hook_views_query_alter().
 *
 * Add date filters from 3 content types: webcast, indico and events.
 */
function cern_event_page_views_query_alter(ViewExecutable $view, QueryPluginBase $query) {
  if ($view->storage->get('id') === 'cern_events' && ($view->current_display === 'feature_events' || $view->current_display === 'past_events')) {
    $date_from = \Drupal::request()->get('date_from');
    $date_to = \Drupal::request()->get('date_to');
    if (!empty($date_from) || !empty($date_to)){
      $query->where[2]['conditions'] = [];
      $query->where[3]['conditions'] = [];
    }
    $date_field_groups = [];
    if (!empty($date_from)) {
      $date_field_groups['event'][0] = ['p_event_display_start_date' => '>='];
      $date_field_groups['indico'][0] = ['indico_event_start_date' => '>='];
      $date_field_groups['webcast'][0] = ['webcast_event_start_date' => '>='];
    }
    if (!empty($date_to)) {
      $date_field_groups['event'][1] = ['p_event_display_end_date' => '<='];
      $date_field_groups['indico'][1] = ['indico_event_end_date' => '<='];
      $date_field_groups['webcast'][1] = ['webcast_event_end_date' => '<='];
    }
    foreach($date_field_groups as $date_field_group){
      $conditions = [];
      foreach ($date_field_group as $key => $fields) {
        $condition_value = $key == 0 ? $date_from :  $date_to;
        foreach ($fields as $field_name => $operator){
          if(!isset($query->tableQueue['node__field_'.$field_name])) {
            $configuration = array(
                'type' => 'LEFT',
                'table' => 'node__field_'.$field_name,
                'field' => 'entity_id',
                'left_table' => 'node_field_data',
                'left_field' => 'nid',
                'operator' => '=',
                'extra' => array(
                    0 => array(
                        'field' => 'deleted',
                        'value' => '0',
                        'numeric' => TRUE
                    ),
                ),
            );
            $join = Views::pluginManager('join')
                ->createInstance('standard', $configuration);
            $query->addTable('node__field_'.$field_name, 'node_field_data', $join);
          }

          $conditions[] = "DATE_FORMAT(node__field_".$field_name.".field_".$field_name."_value, '%Y-%m-%d') ".$operator." DATE_FORMAT('".$condition_value."', '%Y-%m-%d')";
        }
      }
      $conditions = '('.implode(' AND ', $conditions).')';
      $query->addWhereExpression(2, $conditions);
    }
  }


  if ($view->storage->get('id') === 'cern_upcoming_events' || $view->storage->get('id') === 'cern_events' ||
      ($view->storage->get('id') === 'events_microviews' && $view->current_display === 'next_promoted_event_countdown'
          || $view->current_display === 'upcoming_events_collision' || $view->current_display === 'upcoming_events_for_the_media_collision')
  ) {
    if ($view->current_display === 'past_events') {
      $order_direction = 'DESC';
    } else {
      $order_direction = 'ASC';
    }
    // override the ordering
    // this is because the two different date fields are one or the other
    // but not both fields, so we coalesce.
    $field_start_date = "
      CASE
        WHEN type = 'event_page' THEN node__field_p_event_display_start_date.field_p_event_display_start_date_value
        WHEN type = 'indico_event' THEN node__field_indico_event_start_date.field_indico_event_start_date_value
        WHEN type = 'webcast_event' THEN node__field_webcast_event_start_date.field_webcast_event_start_date_value
      END";
    $query->addField(NULL, $field_start_date, 'start_date');
    $query->orderby = array([
        'field' => 'start_date',
        'direction' => $order_direction,
    ]);
  }

  // Alter for the view block "Other Events"
  if ($view->storage->get('id') === 'events') {
    // Category ID value
    $field_category_value = "";
    // Get node
    $node = \Drupal::routeMatch()->getParameter('node');
    // Check if node exists
    if (isset($node)) {
      // Get Content Type (CT)
      $type = $node->getType();
      // If node is Event page CT
      if ($type == "event_page") {
        $field_category_value = \Drupal::routeMatch()->getParameter('node')->get('field_p_event_webcast_indico_id')->value;
        // If node is Indico CT
      } else if ($type == "indico_event") {
        $field_category_value = \Drupal::routeMatch()->getParameter('node')->get('field_indico_event_category_id')->value;
        // If node is Webcast CT
      } else if ($type == "webcast_event") {
        $field_category_value = \Drupal::routeMatch()->getParameter('node')->get('field_webcast_event_category')->value;
      }

      // Adding value for
      foreach ($query->where[2]['conditions'] as $key => $value) {
        $query->where[2]['conditions'][$key]['value'] = $field_category_value;
      }
    }
  }
}


/**
 * Implements hook_views_post_render().
 *
 * Change the title of the events view to All Events.
 */
function cern_event_page_views_post_render(ViewExecutable $view, &$output, CachePluginBase $cache) {
  if ($view->storage->get('id') === 'cern_events' && ($view->current_display === 'feature_events' || $view->current_display === 'feature_events')) {
    $date_from = \Drupal::request()->get('date_from');
    $date_to = \Drupal::request()->get('date_to');
    if (!empty($date_from) || !empty($date_to)) {
      $view->setTitle(t("All events"));
      $view->header['result']->options['content'] = '<a href="/events/past">Past Events</a> | <a class="active" href="/events">All Events (@total)</a>';
    }
  }
}
